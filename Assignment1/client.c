// client code for UDP socket programming 
#include <arpa/inet.h> 
#include <netinet/in.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h> 
#include <unistd.h> 
#include <fcntl.h>

#define IP_PROTOCOL 0 
#define IP_ADDRESS "127.0.0.1" // localhost 
#define PORT_NO 15050 
#define NET_BUF_SIZE 1024
#define cipherKey 'S' 
#define sendrecvflag 0 
#define delim " \t\r\n\a"

char **parse( char *line )
{
    char **tokens = calloc(NET_BUF_SIZE, NET_BUF_SIZE * sizeof(char *));
    char *temp ; 
    
    if(!tokens)
    {
        fprintf(stderr, "ksh: allocation error\n");
    }

    //Splitting line by delim and saving it into temp
    temp = strtok( line , delim );
    // printf("%s\n", temp) ; 
    int i = 0;
    while( temp != NULL )
    {
        // printf("i: %d\n", i) ; 
        tokens[i++] = temp;
        temp = strtok( NULL , delim );
        // printf("%s\n", temp) ; 
    }
    // printf("%d\n", i) ; 
    tokens[i] = NULL ;
    return tokens ; 
}

// funtion to clear buffer 
void clearBuf(char* b) 
{ 
	int i; 
	for (i = 0; i < NET_BUF_SIZE; i++) 
		b[i] = '\0'; 
} 

// function for decryption 
char Cipher(char ch) 
{ 
	return ch; 
} 

// function to receive file 
int recvFile(char* buf, int s) 
{ 
	int i; 
	char ch; 
	for (i = 0; i < s; i++) { 
		ch = buf[i]; 
		// ch = Cipher(ch); 
		if (ch == EOF) 
			break ;
		// else
		// 	printf("%c", ch); 
	} 

	return i; 
} 
int recvFileList(char* buf, int s) 
{ 
	int i; 
	char ch; 
	for (i = 0; i < s; i++) { 
		ch = buf[i]; 
		// ch = Cipher(ch); 
		if (ch == EOF) 
			return 1 ;
		else
			printf("%c", ch); 
	} 

	return 0; 
} 

// driver code 
int main() 
{ 
	int sockfd, nBytes; 
	struct sockaddr_in addr_con; 
	int addrlen = sizeof(addr_con); 
	addr_con.sin_family = AF_INET; 
	addr_con.sin_port = htons(PORT_NO); 
	addr_con.sin_addr.s_addr = inet_addr(IP_ADDRESS); 
	char net_buf[NET_BUF_SIZE]; 
    char list_buf[NET_BUF_SIZE]; 
    char **tokens ; 
	FILE* fp; 

	// socket() 
	sockfd = socket(AF_INET, SOCK_DGRAM, 
					IP_PROTOCOL); 

	if (sockfd < 0) 
		printf("\nfile descriptor not received!!\n"); 
	else
		printf("\nfile descriptor %d received\n", sockfd); 

	while (1) {
        // printf("COMMAND BEING SENT IS %s\n", net_buf) ; 
        clearBuf(net_buf);  
        // printf("COMMAND BEING SENT IS %s\n", net_buf) ;  
		printf("\nPlease enter command:\n"); 
		// scanf("%[^\n]", net_buf); 
        fgets(net_buf, 1024, stdin); 
        printf("NET BUF AT START %s\n", net_buf) ; 
            sendto(sockfd, net_buf, NET_BUF_SIZE, 
			sendrecvflag, (struct sockaddr*)&addr_con, 
			addrlen); 
            

		printf("\n---------Data Received---------\n"); 
        tokens = parse(net_buf) ; 
        if(strcmp(tokens[0],"send") == 0 )
        {   
            // clearBuf(net_buf) ; 
            if(tokens[1] == NULL || tokens[2] != NULL )
            {
                printf("Command should be of the format send <filename>. Note that only one file should be provided\n") ; 
                continue ; 
            }   

            char name[1024] ; 
            strcpy(name, tokens[1]) ;
            int fd_create = open(name, O_WRONLY | O_CREAT | O_TRUNC, 0644); 
            if (fd_create < 0)
            {
                perror("Couldnt open file to write") ; 
            }
            close(fd_create) ; 
            printf("%s\n", tokens[1]) ; 
            while (1) { 
                // receive 
                // printf("INSIDE LOOP\n\n") ;  
                clearBuf(net_buf); 
                nBytes = recvfrom(sockfd, net_buf, NET_BUF_SIZE, 
                                sendrecvflag, (struct sockaddr*)&addr_con, 
                                &addrlen); 

                // process 
                int buffer_length = recvFile(net_buf, NET_BUF_SIZE)  ; 
                printf("FILE CREATION NAME : %s\n", name) ;
                int fd = open(name, O_WRONLY | O_APPEND); 
                if (fd < 0)
                {
                    perror("Couldnt open file to write") ; 
                }
                printf("\nNET_BUF : %s", net_buf) ; 
                // int i = 0 ; 
                // for(i = 0 ; net_buf[i] != EOF ; ++i) ; 
                int sw = write(fd, net_buf, buffer_length);
                if(sw < 0)
                {
                    perror("Write error") ; 
                }
                close(fd) ;  
                // write upto buffer length each time.
                if (buffer_length!= NET_BUF_SIZE) { 
                    break; 
                } 
            } 
        }
        else if(strcmp(tokens[0], "listall") == 0)
        {   
            if(tokens[1] != NULL)
            {
                printf("Command should be of the format 'listall'. With no other arguments\n") ; 
                continue ; 
            }
            // sendto(sockfd, net_buf, NET_BUF_SIZE, 
			// sendrecvflag, (struct sockaddr*)&addr_con, 
			// addrlen); 

            while(1)
            {
                // printf("INSIDE LIST LOOP\n\n") ; 
                clearBuf(net_buf); 
                clearBuf(list_buf) ; 
                nBytes = recvfrom(sockfd, list_buf, NET_BUF_SIZE, 
                                sendrecvflag, (struct sockaddr*)&addr_con, 
                                &addrlen); 
                // printf("=======TEMP \n%s", net_buf) ; 
                // printf("\n----\nBufferLength : %d\n-----\n",buffer_length) ; 
                if (recvFileList(list_buf, NET_BUF_SIZE)) { 
                    break; 
                }  
                clearBuf(list_buf) ; 
                clearBuf(net_buf) ; 
            }

        }
        else{
            printf("Commmand not available\n") ; 
        }
		printf("\n-------------------------------\n"); 
	} 
	return 0; 
} 
