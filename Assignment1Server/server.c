// server code for UDP socket programming 
#include <arpa/inet.h> 
#include <netinet/in.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h> 
#include <unistd.h> 
#include <fcntl.h>
#include <dirent.h> 

#define IP_PROTOCOL 0 
#define PORT_NO 15050 
#define NET_BUF_SIZE 1024 
#define cipherKey 'S' 
#define sendrecvflag 0 
#define nofile "File Not Found!" 
#define delim " \t\r\n\a"

char **parse( char *line )
{
    char **tokens = calloc(NET_BUF_SIZE, NET_BUF_SIZE * sizeof(char *));
    char *temp ; 
    
    if(!tokens)
    {
        fprintf(stderr, "ksh: allocation error\n");
    }

    //Splitting line by delim and saving it into temp
    temp = strtok( line , delim );
    // printf("%s\n", temp) ; 
    int i = 0;
    while( temp != NULL )
    {
        // printf("i: %d\n", i) ; 
        tokens[i++] = temp;
        temp = strtok( NULL , delim );
        // printf("%s\n", temp) ; 
    }
    // printf("%d\n", i) ; 
    tokens[i] = NULL ;
    return tokens ; 
}

// funtion to clear buffer 
void clearBuf(char* b) 
{ 
	int i; 
	for (i = 0; i < NET_BUF_SIZE; i++) 
		b[i] = '\0'; 
} 

// funtion to encrypt 
char Cipher(char ch) 
{ 
	return ch; 
} 

// funtion sending file 
int sendFile(FILE* fp, char* buf, int s) 
{ 
	int i, len; 
	if (fp == NULL) { 
		strcpy(buf, nofile); 
		len = strlen(nofile); 
		buf[len] = EOF; 
		// for (i = 0; i <= len; i++) 
		// 	buf[i] = Cipher(buf[i]); 
		return 1; 
	} 

	// printf("Inside sendfile\n") ; 
	char ch, ch2; 
	for (i = 0; i < s; i++) { 
		ch = fgetc(fp); 
		// ch2 = Cipher(ch);
		// printf("%c %d\n",ch, i) ;  
		buf[i] = ch; 
		if (ch == EOF) 
			return 1; 
	} 
	return 0; 
} 

// driver code 
int main() 
{ 
	int sockfd, nBytes; 
	struct sockaddr_in addr_con; 
	int addrlen = sizeof(addr_con); 
	addr_con.sin_family = AF_INET; 
	addr_con.sin_port = htons(PORT_NO); 
	addr_con.sin_addr.s_addr = INADDR_ANY; 
	char net_buf[NET_BUF_SIZE]; 
	char **tokens ; 
	FILE* fp; 

	// socket() 
	sockfd = socket(AF_INET, SOCK_DGRAM, IP_PROTOCOL); 

	if (sockfd < 0) 
		printf("\nfile descriptor not received!!\n"); 
	else
		printf("\nfile descriptor %d received\n", sockfd); 

	// bind() 
	if (bind(sockfd, (struct sockaddr*)&addr_con, sizeof(addr_con)) == 0) 
		printf("\nSuccessfully binded!\n"); 
	else
		printf("\nBinding Failed!\n"); 

	while (1) { 
		printf("\nWaiting for file name...\n"); 

		// receive file name 
		clearBuf(net_buf); 

		nBytes = recvfrom(sockfd, net_buf, 
						NET_BUF_SIZE, sendrecvflag, 
						(struct sockaddr*)&addr_con, &addrlen); 

		printf("COMMAND RECIEVED : %s\n",net_buf) ;
		tokens = parse(net_buf) ; 
		if(strcmp(tokens[0], "send") == 0)
		{	
			if(tokens[1] == NULL || tokens[2] !=NULL)
			{
                printf("Command should be of the format send <filename>. Note that only one file should be provided\n") ; 				
				continue ; 
			}

			fp = fopen(tokens[1], "r"); 
			printf("\nFile Name Received: %s\n", tokens[1]); 
			if (fp == NULL) 
				printf("\nFile open failed!\n"); 
			else
				printf("\nFile Successfully opened!\n"); 

			while (1) { 
						clearBuf(net_buf); 
				// process 
				if (sendFile(fp, net_buf, NET_BUF_SIZE)) { 
					printf("\t\t----- BEING SENT : %s---------\n", net_buf) ; 
					char temp[NET_BUF_SIZE] ; 
					int i ; 
					for(i = 0 ; net_buf[i] != '\0' ; ++i)
					{
						temp[i] = net_buf[i] ; 
					}
					temp[i] = '\0' ; 
					sendto(sockfd, temp, NET_BUF_SIZE, 
						sendrecvflag, 
						(struct sockaddr*)&addr_con, addrlen); 
					break; 
				} 
				// send 
				sendto(sockfd, net_buf, NET_BUF_SIZE, 
					sendrecvflag, 
					(struct sockaddr*)&addr_con, addrlen); 
						clearBuf(net_buf); 
				printf("\t\t----- BEING SENT : %s---------\n", net_buf) ;
			} 
			if (fp != NULL) 
				fclose(fp); 
		}
		else if(strcmp(tokens[0],"listall") == 0)
		{
			if(tokens[1] != NULL)
            {
                printf("Command should be of the format 'listall'. With no other arguments\n") ; 
                continue ; 
            }
            char currentpath[200];
            char path[200] ; 
            getcwd(currentpath,200);
            struct dirent *pDent ; 
            char list[NET_BUF_SIZE] ; 
            DIR *currentDir = opendir(currentpath);
            if (currentDir == NULL )
            {
            printf("Cannot open directory\n") ;
            return 0; 
            }
            
            char listOfAllFiles[50][50];
            for(int i = 0 ; i < 50 ; ++i)
            {
                for(int j = 0 ; j < 50 ; ++j)
                {
                    listOfAllFiles[i][j] = '\0' ; 
                }
            }

            int j = 0 ; 

            while((pDent = readdir(currentDir)) != NULL )
            {	
                strcpy(listOfAllFiles[j],pDent->d_name);
                ++j;
            }

            int k = 0 ;
            for(int i = 0 ; i < NET_BUF_SIZE ; ++i)
            {
                list[i] = '\0' ; 
            }
            while(k<j)
            {
                // printf("%s \n",listOfAllFiles[k]) ; 
                strcat(list, listOfAllFiles[k]) ; 
                strcat(list, "\n") ; 
                ++k;
            }
			strcat(list, "\t") ; 
			// send list 
			//create temp file, and add list into it. pass list.
			int fd = open("temp.txt", O_WRONLY | O_CREAT | O_TRUNC, 0644); 
			if (fd < 0)
			{
				perror("Couldnt open file to write") ; 
			}
			// printf("\nNET_BUF : %s", net_buf) ; 
			int sw = write(fd, list,NET_BUF_SIZE );
			if(sw < 0)
			{
				perror("Write error") ; 
			}
            close(fd) ; 
			fp = fopen("temp.txt", "r"); 
			printf("\n------\n%s\n------\n", list) ; 
			while (1) { 

				// process 
				if (sendFile(fp, net_buf, NET_BUF_SIZE)) { 
					sendto(sockfd, net_buf, NET_BUF_SIZE, 
						sendrecvflag, 
						(struct sockaddr*)&addr_con, addrlen); 
					break; 
				} 
					// printf("\n\nINSIDE IF\n\n") ; 

				// send 
				sendto(sockfd, net_buf, NET_BUF_SIZE, 
					sendrecvflag, 
					(struct sockaddr*)&addr_con, addrlen); 
				clearBuf(net_buf); 
			} 
			fclose(fp) ; 
			clearBuf(net_buf) ; 
	    }
		else {
			clearBuf(net_buf) ; 
			printf("Command not available") ; 
		} 
	} 
	return 0; 
} 
